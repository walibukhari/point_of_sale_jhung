@extends('layout.main') @section('content')
@if(session()->has('message'))
  <div class="alert alert-success alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ session()->get('message') }}</div> 
@endif
@if(session()->has('not_permitted'))
  <div class="alert alert-danger alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ session()->get('not_permitted') }}</div> 
@endif

<section>
    <div class="table-responsive">
        <table class="table purchase-list" style="width: 100%">
            <thead>
                <tr>
                    <th>{{trans('file.Supplier')}}</th>
                    <th>{{trans('Status')}}</th>
                    <th>{{trans('file.grand total')}}</th>
                    <th>{{trans('Time')}}</th>
                    <th class="not-exported">{{trans('file.action')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $d)
                    @php
                        $supplier = \App\Supplier::where('id','=',$d->supplier_id)->first();
                    @endphp
                    <tr>
                        <th>{{isset($supplier->name) ? $supplier->company_name : 'no supplier'}}</th>
                        <th>{{isset($d->status) && $d->status == 1 ? 'Hold' : ''}}</th>
                        <th>{{$d->grand_total}}</th>
                        <th>{{\Carbon\Carbon::parse($d->created_at)->format('D - m - Y')}} : {{\Carbon\Carbon::parse($d->created_at)->diffForHumans()}}</th>
                        <th>
                            <a href="{{route('unHoldPurchase',[$d->id])}}" class="btn btn-outline-primary">Un Hold Purchase</a>
                        </th>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
@endsection @section('scripts')
<script type="text/javascript" src="https://js.stripe.com/v3/"></script>
@endsection