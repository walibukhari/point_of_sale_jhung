<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHoldPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hold_purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('whare_house')->nullable();
            $table->string('supplier_id')->nullable();
            $table->string('purchase_status')->nullable();
            $table->string('status')->nullable();
            $table->longText('products_json')->nullable();
            $table->double('order_tax')->nullable();
            $table->double('discount')->nullable();
            $table->double('shipping_cost')->nullable();
            $table->double('total_discount')->nullable();
            $table->double('total_tax')->nullable();
            $table->double('total_cost')->nullable();
            $table->double('item')->nullable();
            $table->double('grand_total')->nullable();
            $table->double('paid_amount')->nullable();
            $table->double('total_qty')->nullable();
            $table->longText('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hold_purchases');
    }
}
