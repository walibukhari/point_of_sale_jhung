<?php

namespace App\Http\Controllers;

use App\HoldPurchase;
use App\Product;
use App\Supplier;
use App\Tax;
use App\Warehouse;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class HoldPurchaseController extends Controller
{
    public function holdPurchase(Request $request) {
        $productArray = [];
        foreach ($request->qty as $k => $qty) {
            array_push($productArray,array(
                'qty' => $qty,
                'received' => '',
                'product_code' => '',
                'product_id' => '',
                'product_name' => '',
                'purchase_unit' => '',
                'net_unit_cost' => '',
                'discount' => '',
                'tax_rate' => '',
                'tax' => '',
                'subtotal' => '',
            ));
        }
        foreach ($request->recieved as $k => $received) {
            $productArray[$k]['received'] = $received;
        }
        foreach ($request->product_code as $k => $product_code) {
            $productArray[$k]['product_code'] = $product_code;
        }
        foreach ($request->product_id as $k => $product_id) {
            $productArray[$k]['product_id'] = $product_id;
            $product_name = Product::where('id','=',$product_id)->first();
            $productArray[$k]['product_name'] = $product_name->name;
        }
        foreach ($request->purchase_unit as $k => $purchase_unit) {
            $productArray[$k]['purchase_unit'] = $purchase_unit;
        }
        foreach ($request->net_unit_cost as $k => $net_unit_cost) {
            $productArray[$k]['net_unit_cost'] = $net_unit_cost;
        }
        foreach ($request->discount as $k => $discount) {
            $productArray[$k]['discount'] = $discount;
        }
        foreach ($request->tax_rate as $k => $tax_rate) {
            $productArray[$k]['tax_rate'] = $tax_rate;
        }
        foreach ($request->tax as $k => $tax) {
            $productArray[$k]['tax'] = $tax;
        }
        foreach ($request->subtotal as $k => $subtotal) {
            $productArray[$k]['subtotal'] = $subtotal;
        }
        $product_json = json_encode($productArray);
        $request->request->add([
           'product_json' => $product_json
        ]);
        HoldPurchase::createPurchaseData($request->all());
        return collect([
           'status' => true
        ]);
    }

    public function holdPurchaseList(){
        $data = HoldPurchase::where('status','=',HoldPurchase::HOLD)->get();
        return view('purchase.hold',[
            'data' => $data
        ]);
    }

    public function productWithoutVariant()
    {
        return Product::ActiveStandard()->select('id', 'name', 'code')
            ->whereNull('is_variant')->get();
    }

    public function productWithVariant()
    {
        return Product::join('product_variants', 'products.id', 'product_variants.product_id')
            ->ActiveStandard()
            ->whereNotNull('is_variant')
            ->select('products.id', 'products.name', 'product_variants.item_code')
            ->orderBy('position')->get();
    }

    public function unHoldPurchase($id){
        $data = HoldPurchase::where('id','=',$id)->first();
        $role = Role::find(\Auth::user()->role_id);
        if($role->hasPermissionTo('purchases-add')){
            $lims_supplier_list = Supplier::where('is_active', true)->get();
            $lims_warehouse_list = Warehouse::where('is_active', true)->get();
            $lims_tax_list = Tax::where('is_active', true)->get();
            $lims_product_list_without_variant = $this->productWithoutVariant();
            $lims_product_list_with_variant = $this->productWithVariant();
            $holdData = $data;

            return view('purchase.create', compact('lims_supplier_list', 'lims_warehouse_list', 'lims_tax_list', 'lims_product_list_without_variant', 'lims_product_list_with_variant' , 'holdData'));
        }
        else
            return redirect()->back()->with('not_permitted', 'Sorry! You are not allowed to access this module');
    }

    public function productCodeData($id){
        $data = HoldPurchase::where('id','=',$id)->first();
        return collect([
           'status' => true,
           'data' => json_decode($data->products_json)
        ]);
    }
}
