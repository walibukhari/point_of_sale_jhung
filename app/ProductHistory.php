<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductHistory extends Model
{
    protected $fillable = [
        'name' , 'price' , 'cost_price' , 'qty' , 'purchase_date' , 'total_sale' , 'product_id' ,'combo_product_id'
    ];
    
    public static function createProductHistory($request) {
        // dd($request);
        return self::create([
            'product_id' => $request['product_id'],
            'name' => $request['name'],
            'price'  => $request['price'],
            'cost_price' => $request['cost_price'],
            'qty' => $request['qty'],
            'purchase_date' => $request['purchase_date'], 
            'total_sale' => $request['total_sale'],
            'combo_product_id' => $request['combo_product_id']
        ]);
    }

    public function combo() {
        return $this->hasOne(ProductHistory::class,'combo_product_id','product_id');
    }
}
