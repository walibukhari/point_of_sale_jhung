<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HoldPurchase extends Model
{
    const HOLD = 1;
    const UN_HOLD = 2;
    protected $fillable = [
        'whare_house',
        'supplier_id',
        'purchase_status',
        'status',
        'products_json',
        'order_tax',
        'discount',
        'shipping_cost',
        'paid_amount',
        'grand_total',
        'item',
        'total_cost',
        'total_tax',
        'total_discount',
        'total_qty',
        'note',
    ];

    public static function createPurchaseData($data){
        self::create([
            'note' => $data['note'],
            'whare_house' => $data['warehouse_id'],
            'supplier_id' => $data['supplier_id'],
            'purchase_status' => $data['status'],
            'status' => self::HOLD,
            'products_json' => $data['product_json'],
            'order_tax' => $data['order_tax'],
            'discount' => $data['order_discount'],
            'shipping_cost' => $data['shipping_cost'],
            'paid_amount' => $data['paid_amount'],
            'grand_total' => $data['grand_total'],
            'item' => $data['item'],
            'total_cost' => $data['total_cost'],
            'total_tax' => $data['total_tax'],
            'total_discount' => $data['total_discount'],
            'total_qty' => $data['total_qty']
        ]);
    }
}
